package fr.idak.tuto.jpademo.repository;

import fr.idak.tuto.jpademo.ApplicationTest;
import fr.idak.tuto.jpademo.domain.Article;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@ApplicationTest
@DataJpaTest
public class ArticleRepositoryTest {
    @Autowired
    private ArticleRepository articleRepository;

    @Test
    @Sql(statements = {"INSERT INTO ARTICLE(EAN, TITLE, PRICE) VALUES('9853245679056','Article 1', 12.49)"})
    public void shouldFindArticleByEan(){
        Optional<Article> article = articleRepository.findByEan("9853245679056");
        Assertions.assertThat(article).isNotEmpty();
        Assertions.assertThat(article.get()).extracting("ean","title","price")
                .containsExactlyInAnyOrder("9853245679056","Article 1", 12.49);
    }

    @Test
    @Sql(statements = {"INSERT INTO ARTICLE(EAN, TITLE, PRICE) VALUES('9853245679056','Article 1', 12.49)"})
    public void shouldFindArticleByEan_notFound(){
        Optional<Article> article = articleRepository.findByEan("9853245679054");
        Assertions.assertThat(article).isEmpty();
    }
}