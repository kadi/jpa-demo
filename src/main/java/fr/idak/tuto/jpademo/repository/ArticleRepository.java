package fr.idak.tuto.jpademo.repository;

import fr.idak.tuto.jpademo.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArticleRepository  extends JpaRepository<Article, Long> {
    Optional<Article> findByEan(String ean);
}
